# Vagrant Puppet

Repo for provisioning Vagrant with Puppet. 
Installs the following: Apache, wget, curl, git, vim (with my own dotfiles),
latest PHP, Composer, PostgreSQL (or MySQL), Node with Bower and Gulp.
It also sets a custom message of the day.

## Vagrant config
Vagrant is set up to use Ubuntu Precise 32-bit and NFS sync. The following port
forwards are set: 

- 80 to 8080
- 5432 to 15432
- 3306 to 13306

## Install instructions

- clone and modify the `Vagrantfile`. Out of the box it tries to sync the parent
directory's `project` directory. 
- edit the default manifest located in `puppet/manifests/default.pp` 
- run `vagrant up`

## The following Puppetlabs modules are included:
- apt
- concat
- firewall
- postgresql
- stdlib
