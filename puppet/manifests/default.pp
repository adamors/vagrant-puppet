include motd
#class { 'coreutils': vim => false }
include 'ppa-load'
#include apache2
class { 'mysql': }
include nodejs
class { 'my-postgresql': 
  db      => 'vagrant',
  db_user => 'vagrant',
  db_pass => 'vagrant',
}

package { 'libqt4-dev': ensure => present }
package { 'xvfb': ensure       =>  present }
