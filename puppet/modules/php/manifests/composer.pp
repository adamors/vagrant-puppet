class php::composer {
  include coreutils

  exec { 'composer': 
    command => '/usr/bin/curl -sS https://getcomposer.org/installer | php',
    require   => [Package['php5'], Package['curl']],
  }

  exec { 'composer-globalize':
    command => '/bin/mv composer.phar /usr/bin/composer',
    require => Exec['composer'], 
  }
}
