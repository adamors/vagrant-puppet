class php ( $db = 'mysql' ) {

  include apache2

  package { 'php5':
    ensure  => installed,
    require => [Class['ppa-load'], Package['apache2']],
  }

  file { 'php-cli-ini':
    ensure  => file,
    path    => '/etc/php5/cli/php.ini',
    source  => 'puppet:///modules/php/php.cli.dev.ini',
    replace => true,
    require => Package['php5'],
  }

  file { 'php-apache-ini':
    ensure  => file,
    path    => '/etc/php5/apache2/php.ini',
    source  => 'puppet:///modules/php/php.apache.dev.ini',
    replace => true,
    require => Package['php5'],
    notify  => Service['apache2'],
  }

  package { ['libapache2-mod-php5', 'php5-curl', 'php5-gd', 'php5-mcrypt']:
    ensure  => installed,
    require => Package['php5'],
    notify  => Service['apache2'],
  }

  if $db == 'mysql' {
    include mysql

    package {'php5-mysql':
      ensure => installed,
      require => [Package['php5'], Package['mysql-server']],
      notify  => Service['apache2'],
    }
  } elsif $db == 'pgsql' {
    package {'php5-pgsql':
      ensure  => installed,
      require => [Package['php5'], Class['postgresql::server']],
      notify  => Service['apache2'],
    }
  }
  
  class { 'php::composer': }
}
