class ppa-load {
  include apt
  apt::ppa { 'ppa:ondrej/php5': }
  apt::ppa { 'ppa:chris-lea/node.js': }
  exec { 'apt-update':
      command => '/usr/bin/apt-get update'
  }
}
