class nodejs {

    package { 'nodejs':
      ensure  => installed,
      require => Class['ppa-load']
    }

    exec { 'install-gulp-bower':
      command => '/usr/bin/npm -g install bower gulp',
      require  => Package['nodejs'],
    }
}

